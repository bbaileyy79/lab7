/*
Lab 6
Name: Breanna Bailey
Clemson Username: Bjbaile
CUID: C92102788
Lab Section: 001
Lecture Section: 010
T.A: Nushrat Humaira
Description: This code is using c++ to shuffle through and sort a list
of names based on user input
*/



#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));


  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee arr1[11];
	/*Uses for loop to get user input of employee details and saves
	it in an array, passing through the struct*/
	for (int i=0;i<10;++i) {
		cout<<"Enter employee first name: ";
		cin>>arr1[i].firstName;
		cout<<"Enter employee last name: ";
		cin>>arr1[i].lastName;
		cout<<"Enter employee birth year: ";
		cin>>arr1[i].birthYear;
		cout<<"Enter employee hourly wage: ";
		cin>>arr1[i].hourlyWage;
		cout<<"\n";
	}
	cout<<"\n";

  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
	 random_shuffle(arr1, arr1+10, myrandom);


   /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/
		employee arr2[6];
		for (int i=0;i<5;++i) {
			arr2[i] = arr1[i];
		}


    /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
		 std::sort(arr2,arr2+5,name_order);


    /*Now print the array below */
		//Uses for loop to print the information from the new, sorted array
		for (int i=0;i<5;++i) {
			cout<<setw(10)<<right<<arr2[i].lastName + ", " + arr2[i].firstName<<endl;
			cout<<setw(10)<<right<<arr2[i].birthYear<<endl;
			cout<<setw(10)<<right<<fixed<<setprecision(2)<<arr2[i].hourlyWage<<endl;
			cout<<"\n";
		}



  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
  // IMPLEMENT
	if (lhs.lastName < rhs.lastName) {
		return true;
	}
	return false;
}
